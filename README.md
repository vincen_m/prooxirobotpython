# ProoXi Bot

## Détails

   Permet de scanner les sites contenus dans la base de donnée dont les catégories ont plus de 10 mots-clefs

## Lancement

   scrapy crawl ProoXi

## Exécution

   Le robot ProoXi récupère les catégories qui ont plus de 10 mots-clefs grâce à la requêtes suivante:

   `SELECT C.categorie 
      FROM 
         px_cat_2_mc CM 
      RIGHT JOIN 
         px_categorie C 
      ON 
         C.id = CM.id_categorie 
      GROUP BY 
         C.id 
      HAVING 
         count(CM.id_motclef) >= 10`
      
   Ensuite il intérroge la base de donnée pour récupérer les sites de chaque catégorie avec la requête suivante : 
      
      `SELECT px_boutique.url                                                                                                                                                               
         FROM                                                                                                                                                                                 
            px_categorie 
         INNER JOIN                                                                                                                                                          
            px_cat_2_boutique
         ON 
            px_categorie.id = px_cat_2_boutique.id_categorie                                                                                                            
         INNER JOIN                                                                                                                                                                           
            px_boutique ON px_boutique.id = px_cat_2_boutique.id_boutique
         WHERE                                                                                                                                                                                
            px_categorie.categorie = \"""" + categorieTmp + """\" 
         AND 
            px_boutique.url != ""                                                                                                  
         AND 
            px_boutique.url != "http://"`
   
   Après avoir scanné les sites contenu dans la base de donnée, Scrapy extrait les données suivante :
   
   * URL
   * NOM
   * DESCRIPTION
   * MOTS CLEFS
   * STATUS
   * H2
      
   Scrapy vérifie que le site se trouve dans la bonne catégorie. Le cas échéant il le déplace dans une autre table :
   
   `px_boutique_tmp3`